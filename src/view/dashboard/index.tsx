import { Container } from "components";
import { NavLink, Outlet, useLocation } from "react-router-dom";

export default function Dashboard () {
    const paths = ['/example', '/focus-management', '/infinite-query']
    const location = useLocation();
    if (location.pathname !== '/') return <Outlet />
    return (
        <Container>
            {paths.map(route => (<p><NavLink to={route}>{route.slice(0)}</NavLink></p>))}
        </Container>
    )
}