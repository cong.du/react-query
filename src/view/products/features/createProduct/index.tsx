import { useMutation, useQueryClient } from "@tanstack/react-query";
import { QUERY_KEY, apis } from "assets/constants";
import { Container } from "components";
import { Fragment, useState } from "react";
import { useNavigate } from "react-router-dom";

interface IFormData {
  [key: string]: any;
}

export default function CreateProduct () {
  const [form, setForm] = useState<IFormData>({});
  const queryClient = useQueryClient();
  const navigate = useNavigate();

  const data = queryClient.getQueryData([QUERY_KEY.PRODUCTS])

  const { mutate, variables, isPending } = useMutation({
    mutationFn: apis.createProduct,
    onMutate: (variables) => {
      console.log('variables ', variables)
      return { id: 1 }
    },
    onError: (error, variables, context) => {
      // An error happened!
      console.log(`rolling back optimistic update with id ${context?.id}`)
    },
    onSuccess: (data: any, variables, context) => {
      // Boom baby!
      console.log('data, variables, context ', { data, variables, context })
      // update key products when add new a product
      // queryClient.invalidateQueries({ queryKey: [QUERY_KEY.PRODUCTS] });
      // update new product to cache
      queryClient.setQueryData([QUERY_KEY.PRODUCTS, { id: data?.id || 0 }], data);
      // navigate('/products')
    },
    onSettled: (data, error, variables, context) => {
      // Error or success... doesn't matter!
    },
  });

  const handleSubmit = (e: any) => {
    e.preventDefault();
    mutate(form);
  }

  const handleChange = (e: any) => {
    const { value, name } = e.target;
    setForm(prev => ({ ...prev, [name]: value }))
  }

  return (
    <Container status="success">
      <form onSubmit={handleSubmit}>
        <label htmlFor="title">Title: <input type="text" onChange={handleChange} value={form?.title} name="title" id="title" /></label>
        <button type="submit">Submit</button>
      </form>
      {isPending && <li style={{ opacity: 0.5 }}>{variables?.title}</li>}
      <p>Products</p>
      {(data as any)?.pages.map((group: any, i: number) => (
          <Fragment key={i}>
          {group.products.map((project: any) => (
              <p key={project.id}>{project.title}</p>
          ))}
          </Fragment>
      ))}
    </Container>
  )
}