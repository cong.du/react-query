import { useQuery, useQueryClient } from "@tanstack/react-query"
import { QUERY_KEY } from "assets/constants";
import { getProductDetailGroup } from "assets/constants/query";
import { Container } from "components";
import { useParams } from "react-router-dom";

interface IProduct {
  id?: number;
  price?: number;
  thumbnail?: string;
  title?: string;
  description?: string;
  brand?: string;
}

export default function DetailProduct () {
  const params = useParams();
  const queryClient = useQueryClient();
  const queryDetail = getProductDetailGroup(params?.id || 0);
  const { status } = useQuery({
    ...queryDetail,
    initialData: () => {
      const state: any = queryClient.getQueryState([QUERY_KEY.PRODUCTS]);
      console.log('state ', state);
      if (state && Date.now() - state.dataUpdatedAt <= 10 * 1000) {
        return state.data.find((d: any) => Number(d.id) === Number(params?.id))
      }
    }
  });

  const product = queryClient.getQueryData<IProduct>(queryDetail.queryKey);

  return (
    <Container status={status}>
      <img src={product?.thumbnail || ''} alt={product?.title} />
      <p><span>{product?.title} ({product?.brand}) - {product?.price}$</span></p>
      <p>{product?.description}</p>
    </Container>
  )
}