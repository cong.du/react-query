import { keepPreviousData, useInfiniteQuery } from "@tanstack/react-query";
import { QUERY_KEY, apis } from "assets/constants";
import axios from "axios";
import { Container } from "components";
import { NavLink } from "react-router-dom";
import { Fragment } from "react/jsx-runtime";

export default function ProductList () {
    const {
        data,
        error,
        fetchNextPage,
        hasNextPage,
        isFetching,
        isFetchingNextPage,
        status,
    } = useInfiniteQuery({
        queryKey: [QUERY_KEY.PRODUCTS],
        queryFn: (data) => {
          const cancelToken = axios.CancelToken;
          const source = cancelToken.source();
          const promise = apis.getProducts({ skip: data.pageParam, limit: 10, offset: 3 }, data.signal)
          data.signal?.addEventListener('abort', () => {
            source.cancel('Query was cancelled by TanStack Query');
          })
          return promise;
        },
        placeholderData: keepPreviousData,
        initialPageParam: 0,
        getNextPageParam: (page, lastPage) => {
            const [obj] = (page as any)?.products?.slice(-1);
            return obj?.id || 0
        },
    })

    return (
        <Container>
          <p><NavLink to="/products/create">Add new Product</NavLink></p>
            {(data as any)?.pages.map((group: any, i: number) => (
                <Fragment key={i}>
                {group.products.map((project: any) => (
                    <p key={project.id}>{project.title}</p>
                ))}
                </Fragment>
            ))}
            <div>
                <button
                onClick={() => fetchNextPage()}
                // disabled={!hasNextPage || isFetchingNextPage}
                >
                {isFetchingNextPage
                    ? 'Loading more...'
                    : hasNextPage
                    ? 'Load More'
                    : 'Nothing more to load'}
                </button>
            </div>
            <div>{isFetching && !isFetchingNextPage ? 'Fetching...' : null}</div>
        </Container>
    )
}