import { useQueries, useQuery, useQueryClient, useSuspenseQuery } from "@tanstack/react-query";
import { query } from "assets/constants";
import { Container } from "components";
import { NavLink, useParams } from "react-router-dom";

export default function DetailExample () {
    const params = useParams();
    const queryClient = useQueryClient();
    const queryGroupDetail = query.getDetailTodo(params?.id);

    const { isLoading } = useQuery(queryGroupDetail as any)
    const data = queryClient.getQueryData<any>(queryGroupDetail?.queryKey)
    const ids = [1,2,3,4];
    useQueries({
        queries: ids.map(id => query.getDetailTodo(id) as any)
    })
    const allData = ids.map(id => queryClient.getQueryData<any>(query.getDetailTodo(id).queryKey));
    console.log('allData ', allData)

    // useSuspenseQuery(query.getDetailTodo(5) as any)
    // queryClient.prefetchQuery(query.getDetailTodo(23) as any)
    

    const defail = () => {
        const content = [];
        for(const key in data) {
            content.push(<p>{key}: {data[key].toString()}</p>)
        }
        return content;
    }
    return (
        <Container isLoading={isLoading}>
            <NavLink to="/example">Go to back</NavLink>
            {defail()}
        </Container>
    )
}