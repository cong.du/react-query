import { useMutation, useQuery, useQueryClient, skipToken } from "@tanstack/react-query";
import { apis, query } from "assets/constants";
import { Container } from "components";
import { NavLink } from "react-router-dom";

interface ITodo {
    completed?: boolean;
    id?: number;
    title?: string;
    userId?: number;
}

export default function Todos() {
    // Access the client
    const queryClient = useQueryClient()
    const queryTodo = query.getTodos();
  
    // Queries
    const { isLoading, refetch, isFetching } = useQuery(queryTodo)
    const data = queryClient.getQueryData<ITodo[]>(queryTodo.queryKey)
  
    // Mutations
    const mutation = useMutation({
      mutationFn: apis.postTodo,
      onSuccess: () => {
        // Invalidate and refetch
        queryClient.invalidateQueries({ queryKey: queryTodo.queryKey })
      },
    })
  
    return (
        <Container>
            <NavLink to="/">Go to Home</NavLink>
            <p><button onClick={() => refetch()} disabled={isLoading}>Refetch data</button></p>
            <Container isLoading={isLoading} isFetching={isFetching}>
             <div>
                <ul>{(data || []).map((todo: ITodo) => <li key={todo.id}>{todo.title} <NavLink to={`/example/${todo.id}`}>Go to detail</NavLink></li>)}</ul>
        
                <button
                onClick={() => {
                    mutation.mutate({
                    id: Date.now(),
                    title: 'Do Laundry',
                    } as any)
                }}
                >
                Add Todo
                </button>
            </div>
        </Container>
        </Container>
    )
  }