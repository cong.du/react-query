import styled from "@emotion/styled";
import { useQuery, useQueryClient } from "@tanstack/react-query";
import { getProductsGroup } from "assets/constants/query";
import { Container, Paginate, Table } from "components";
import { IColumnType } from "components/table/type";
import request from "service/axios";

interface IData {
    fullName: string;
    role: string;
    tags: string[];
}

const columns: IColumnType<IData>[] = [
    {
      key: "fullName",
      title: "Full Name",
    },
    {
      key: "role",
      title: "Role",
    },
    {
      key: "tags",
      title: "Tags",
      render: (_, { tags }) => (
        <>
          {tags.map((tag, tagIndex) => (
            <Span key={`tag-${tagIndex}`} style={{ marginLeft: tagIndex * 4 }}>
              {tag}
            </Span>
          ))}
        </>
      ),
    },
  ];

  const data: IData[] = [
    {
      fullName: "Francisco Mendes",
      role: "Full Stack",
      tags: ["dev", "blogger"],
    },
    {
      fullName: "Ricardo Malva",
      role: "Social Media Manager",
      tags: ["designer", "photographer"],
    },
  ];

const Span = styled.span`
    background-color: #596b7e;
    color: white;
    padding-left: 10px;
    padding-right: 10px;
    border-radius: 4px;
`;

// not use due api has not key currentPage
export default function PaginationPage () {
  const queryClient = useQueryClient();
  const productsGroup = getProductsGroup({ limit: 10, skip: 5 });
  const data2 = useQuery(productsGroup);
  console.log('data ', data2);
  const products = queryClient.getQueryData<any>(productsGroup.queryKey)
  return (
    <Container>
      <Table
        data={data}
        columns={columns}
        paginate={{ pageCount: 10 }}
        rowKey="fullName"
      />
    </Container>
  )
}