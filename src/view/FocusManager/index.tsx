import { focusManager } from "@tanstack/react-query";
import { Container } from "components";
import { NavLink } from "react-router-dom";

export default function FocusManager () {
    focusManager.setEventListener((handleFocus) => {
        // Listen to visibilitychange
        if (typeof window !== 'undefined' && window.addEventListener) {
            window.addEventListener('visibilitychange', () => handleFocus(), false)
            return () => {
            // Be sure to unsubscribe if a new handler is set
            window.removeEventListener('visibilitychange', () => handleFocus())
            }
        }
    })
    return (
        <Container>
            <NavLink to="/">Go to Home page</NavLink>
            <br />
            <input type="text" />
        </Container>
    )
}