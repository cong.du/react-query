
interface IContainerProps {
    isError?: boolean;
    isPending?: boolean;
    isLoading?: boolean;
    isFetching?: boolean;
    isLoadingError?: boolean;
    isRefetchError?: boolean;
    status?: 'success' | 'pending' | 'error';
    error?: any;
    children: React.ReactNode;
}

export default function Container ({ status, error, isError = false, isPending = false, isLoading = false, isFetching = false, isLoadingError = false, isRefetchError = false, children }: IContainerProps): JSX.Element {
    switch(true) {
        case status === 'error':
        case isError:
            return <>{error?.message}</>;
        case isLoading:
            return <>Loading...</>
        case isPending:
        case status === 'pending':
            return <>Pending...</>
        case isLoading:
            return <>Loading...</>
        case isLoadingError:
            return <>Loading Error...</>
        case isRefetchError:
            return <>Refetching Error...</>
        case isFetching:
            return <>Fetching...</>
        default:
            return <>{children}</>
    }
}
