import ReactPaginate, { ReactPaginateProps } from "react-paginate"
import styled from "@emotion/styled"

export interface IPaginate extends ReactPaginateProps {}

export default function Paginate ({ ...rest }: IPaginate) {
    return (
        <RP
            {...rest}
        />
    )
}

const RP = styled(ReactPaginate)`
    width: auto;
    display: flex;
    flex-direction: row;
    list-style-type: none;
    padding: 0px;
    margin: 12px 0 0;

    li.previous a, li.next a, li a {
        z-index: 3;
        position: relative;
        display: block;
        color: #0d6efd;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #dee2e6;
        transition: color .15s ease-in-out, background-color .15s ease-in-out, border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        padding: .375rem .75rem;
        margin-left: -1px;
        cursor: pointer;
        user-select: none;

        :hover {
            z-index: 2;
            color: #0a58ca;
            background-color: #e9ecef;
            border-color: #dee2e6;
        }
    }
    li.selected a {
    background-color: #0366d6;
    border-color: transparent;
    color: white;
    min-width: 32px;
    }
    li.disable, li.disabled a {
        cursor: not-allowed;
        color: #6c757d;
        background-color: #fff;
        border-color: #dee2e6;
    }
    li.previous a {
        border-top-left-radius: .25rem;
        border-bottom-left-radius: .25rem;
    }
    li.next a {
        border-top-right-radius: .25rem;
        border-bottom-right-radius: .25rem;
    }
`;