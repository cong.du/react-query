import styled from "@emotion/styled";
import { IColumnType } from "./type";
import get from "lodash.get";

interface ITableRowCell<T> {
    item: T;
    column: IColumnType<T>;
    className?: string;
}

const TableCell = styled.td`
    padding: 12px;
    font-size: 14px;
    color: grey;
    border: 1px solid #f1f1f1;
`;

export default function TableRowCell<T>({ item, column, className }: ITableRowCell<T>): JSX.Element {
    const value = get(item, column.key);

    return (
      <TableCell className={className}>{(column.render ? column.render(column, item) : value) as any}</TableCell>
    );
  }