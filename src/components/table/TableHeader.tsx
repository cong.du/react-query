import styled from "@emotion/styled";
import { IColumnType } from "./type";

interface ITableHeaderProps<T> {
    columns: IColumnType<T>[];
}

export default function TableHeader<T> ({ columns }: ITableHeaderProps<T>) {
    return (
        <tr>
            {columns.map((column, columnIndex) => (
                <TableHeaderCell
                    key={`table-head-cell-${columnIndex}`}
                    style={{ width: column.width }}
                >
                    {column.title}
                </TableHeaderCell>
            ))}
        </tr>
    )
}

const TableHeaderCell = styled.th`
    background-color: #dee3f0;
    padding: 12px;
    font-weight: 500;
    text-align: left;
    font-size: 14px;
    color: #2c3e50;
    border: 1px solid #f1f1f1;

    &:first-of-type {
        border-top-left-radius: 8px;
    }
    &:last-child {
        border-top-right-radius: 8px;
    }
`;