import styled from "@emotion/styled";
import { IColumnType } from "./type";
import TableRowCell from "./TableRowCell";

interface ITableRowProps<T> {
    data: T[];
    columns: IColumnType<T>[];
    rowKey?: keyof T | string;
}

const TableRowItem = styled.tr`
    cursor: auto;

    &:nth-of-type(odd) {
        background-color: #f9f9f9;
    }
    &:nth-last-of-type() {
        border-bottom-left-radius: 12px;
        border-bottom-right-radius: 12px;
    }
`;

export default function TableRow<T>({ data, columns, rowKey }: ITableRowProps<T>): JSX.Element {
    return (
      <>
        {data.map((item, itemIndex) => (
          <TableRowItem key={`table-body-${(item as any)[rowKey] ?? itemIndex}`}>
            {columns.map((column, columnIndex) => (
              <TableRowCell
                key={`table-row-cell-${columnIndex}`}
                item={item}
                column={column}
              />
            ))}
          </TableRowItem>
        ))}
      </>
    );
  }