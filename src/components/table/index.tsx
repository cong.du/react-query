import styled from "@emotion/styled";
import { IColumnType } from "./type";
import TableHeader from "./TableHeader";
import TableRow from "./TableRow";
import Paginate, { IPaginate } from "components/paginate";
import { useMemo } from "react";

interface ITablePaginate extends IPaginate {
    total?: number;
}

interface ITableProps<T> {
    data: T[];
    columns: IColumnType<T>[];
    isPagination?: boolean;
    paginate?: Partial<ITablePaginate>;
    rowKey?: keyof T | string;
}

export default function Table<T> ({ data, columns, isPagination = true, paginate, rowKey }: ITableProps<T>): JSX.Element {
    const pagination = useMemo(() => {
        if (!isPagination || !paginate?.pageCount) return;

        return <Paginate pageCount={paginate?.pageCount || 0} {...paginate} />
    }, [data, paginate])

    const totalItemPerPage = useMemo(() => {
        return (
            <div className="of-page">{paginate?.total || 0} of {paginate?.pageCount || 0} pages</div>
        );
    }, []);

    return (
        <div>
            <TableWrapper>
                <thead>
                    <TableHeader columns={columns} />
                </thead>
                <tbody>
                    <TableRow data={data} columns={columns} rowKey={rowKey} />
                </tbody>
            </TableWrapper>
            <TableFooter>
                {totalItemPerPage}
                {pagination}
            </TableFooter>
        </div>
    )
}

const TableWrapper = styled.table`
    border-collapse: collapse;
    border: none;
    width: 100%;
`;

const TableFooter = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;

    .of-page {
        color: #404040;
        font-size: 12px;
    }
`;