import { createBrowserRouter, Outlet, RouterProvider } from "react-router-dom";
import FocusManager from "view/FocusManager";
import CreateProduct from "view/products/features/createProduct";
import Dashboard from "view/dashboard";
import DetailProduct from "view/products/features/detailProduct";
import Todos from "view/example";
import DetailExample from "view/example/Detail";
import InfiniteQuery from "view/infiniteQuery";
import PaginationPage from "view/paginate";
import ProductList from "view/products";

const NotFoundPage = () => (<>Not Found</>)

const router = createBrowserRouter([
    {
        path: '/',
        element: <Dashboard />,
        children: [
            {
                path: 'example',
                element: <Outlet />,
                children: [
                  {
                    index: true,
                    element: <Todos />,
                  },
                  {
                    path: ':id',
                    element: <DetailExample />,
                  }
                ]
            },
            {
              path: 'focus-management',
              element: <FocusManager />
            },
            {
              path: 'infinite-query',
              element: <InfiniteQuery />
            },
            {
              path: 'paginate',
              element: <PaginationPage />
            },
            {
              path: 'products',
              element: <Outlet />,
              children: [
                {
                  index: true,
                  element: <ProductList />
                },
                {
                  path: ':id',
                  element: <DetailProduct />
                },
                {
                  path: 'create',
                  element: <CreateProduct />
                }
              ]
            },
        ]
    },
    {
      path: '*',
      element: <NotFoundPage />,
    },
],
{
  basename: "/"
})

export default function RoutesProvider () {
    return <RouterProvider router={router} future={{ v7_startTransition: true }} />
}
