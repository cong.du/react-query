import { QueryClient, keepPreviousData, queryOptions } from "@tanstack/react-query";
import { QUERY_KEY } from "./query-key";
import { apis } from ".";

const DEFAULT_QUERY = {
    queryKey: [''],
    queryFn: () => {},
}

export function getTodos() {
    return queryOptions({
        queryKey: [QUERY_KEY.TODOS],
        queryFn: apis.getTodos,
        staleTime: 5 * 1000,
        // enable focus page is refesh query
        refetchOnWindowFocus: true
    });
}

export function postTodos(data: any) {
    return queryOptions({
        queryKey: [QUERY_KEY.TODOS],
        queryFn: () => apis.postTodo(data),
        staleTime: 5 * 1000
    });
}

export function getDetailTodo(id?: string | number) {
    if (!id) return queryOptions(DEFAULT_QUERY);
    return queryOptions({
        queryKey: [QUERY_KEY.DETAIL_TODO, id],
        queryFn: (data) => {
            console.log('data ', data)
            return apis.getTodoById(id)
        },
        staleTime: 5 * 1000
    });
}

export function getProductsGroup(params?: any) {
    return queryOptions({
        queryKey: [QUERY_KEY.PRODUCTS, params],
        queryFn: ({ signal }) => apis.getProducts(params, signal),
        placeholderData: keepPreviousData,
    });
}

export function getProductDetailGroup(id: number | string) {
    return queryOptions({
        queryKey: [QUERY_KEY.PRODUCT_DETAIL, id],
        queryFn: () => apis.getProductById(id),
    });
}
