export const QUERY_KEY = {
    TODOS: 'todos',
    DETAIL_TODO: 'todos/detail',
    PRODUCTS: 'products',
    PRODUCT_DETAIL: 'product/detail'
}