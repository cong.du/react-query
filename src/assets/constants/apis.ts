import { AxiosResponse } from "axios";
import request from "service/axios";

const getTodos = (): Promise<AxiosResponse<any>> => request('todos');
const getTodoById = (id: string | number): Promise<AxiosResponse<any>> => request(`todos/${id}`);
const postTodo = (data: any) => request('posts', { method: 'POST', data });
const getProducts = (params: any, signal: any): Promise<AxiosResponse<any>> => request('/products', { params, signal });
const getProductById = (id: string | number): Promise<AxiosResponse<any>> => request(`/products/${id}`);
const createProduct = (data: any): Promise<AxiosResponse<any>> => request(`/products/add`, { method: 'POST', data });

export {
    getTodos, postTodo, getTodoById, getProducts, getProductById, createProduct
}