import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools'
import RoutesProvider from './routes';

// create a client
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      // default is true, If a user leaves your application and returns and the query data is stale
      refetchOnWindowFocus: false, // default: true
      // retry api 10 times when fail
      retry: 10,
      // retry default delay 30 miliseconds
      retryDelay: (attemptIndex) => Math.min(1000 * 2 ** attemptIndex, 30000),
    },
  },
});

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <RoutesProvider />
      <ReactQueryDevtools />
    </QueryClientProvider>
  );
}

export default App;
